//
//  SerialInterface.h
//  WirelessClearComm
//
//  Created by Gregory Bell on 11/26/18.
//  Copyright © 2018 Gregory Bell. All rights reserved.
//

#ifndef SerialInterface_h
#define SerialInterface_h

#if defined (raspi)

extern "C" {
#include <asm/termbits.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
}

class serialInterface{
private:
    string serialDeviceName;
    int handle;
    int baud;
    
    
    
public:
    serialInterface(){}
    
    serialInterface(string serialDeviceName, int baud)
    {
        handle=-1;
        open(serialDeviceName,baud);
    }
    
    ~serialInterface()
    {
        if(handle >=0)
            close();
    }
    
    void close(void)
    {
        if(handle >=0)
            ::close(handle);
        handle = -1;
    }
    
    
    bool open(string serialDeviceName , int baud)
    {
        struct termios tio;
        struct termios2 tio2;
        this->serialDeviceName=serialDeviceName;
        this->baud=baud;
        handle  = open(this->serialDeviceName.c_str(),O_RDWR | O_NOCTTY /* | O_NONBLOCK */);
        
        if(handle <0)
            return false;
        tio.c_cflag =  CS8 | CLOCAL | CREAD;
        tio.c_oflag = 0;
        tio.c_lflag = 0;       //ICANON;
        tio.c_cc[VMIN]=0;
        tio.c_cc[VTIME]=1;     // time out every .1 sec
        ioctl(handle,TCSETS,&tio);
        
        ioctl(handle,TCGETS2,&tio2);
        tio2.c_cflag &= ~CBAUD;
        tio2.c_cflag |= BOTHER;
        tio2.c_ispeed = baud;
        tio2.c_ospeed = baud;
        ioctl(handle,TCSETS2,&tio2);
        
        //   flush buffer
        ioctl(handle,TCFLSH,TCIOFLUSH);
        
        return true;
    }
    
    bool isOpen(void)
    {
        return( handle >=0);
    }
    
    bool send( unsigned char  * data,int len)
    {
        if(!isOpen()) return false;
        int rlen= write(handle,data,len);
        return(rlen == len);
    }
    
    bool send( unsigned char value)
    {
        if(!isOpen()) return false;
        int rlen= write(handle,&value,1);
        return(rlen == 1);
    }
    
    bool send(std::string value)
    {
        if(!isOpen()) return false;
        int rlen= write(handle,value.c_str(),value.size());
        return(rlen == value.size());
    }
    
    
    int  receive( unsigned char  * data, int len)
    {
        if(!isOpen()) return -1;
        
        // this is a blocking receives
        int lenRCV=0;
        while(lenRCV < len)
        {
            int rlen = read(handle,&data[lenRCV],len - lenRCV);
            lenRCV+=rlen;
        }
        return  lenRCV;
    }
    
    bool numberByteRcv(int &bytelen)
    {
        if(!isOpen()) return false;
        ioctl(handle, FIONREAD, &bytelen);
        return true;
    }
    friend class masterClass;
    
};

#endif

#endif /* SerialInterface_h */
