//  Created by Gregory Bell on 10/23/18.
//  Copyright © 2018 Gregory Bell. All rights reserved.
//

#pragma once
#include <vector>
#include <thread>
#include "GPIO.hpp"
#include "RF24/RF24.h"
#include "RF24/utility/RPi/bcm2835.h"
#include "SerialInterface.h"
/*
 Attena Pins
 ce 26
 csn 19
 irq 13
 mosi 10
 miso 9
 sck 11
 */

using namespace std;

static uint8_t addresses[][6] = {"1Node","2Node","3Node", "4Node", "5Node", "6Node" };//I dunno why I can't declare an array in a class but I can't sooo

class clientClass{
private:
    //Audio byte array
    vector<uint8_t> byteArray;
    //Threads
    thread modeSetterThread;
    thread rxListener;
    
    //The list GPIO pins being used.. Also once the hardware list is finilaized this will control the LEDs
    //Attena pins NOTE: These are the GPIO numbers not the pin numbers
    
    RF24 *radio;
    //LED pins
    
    GPIO* ledRed;//4
    GPIO* ledGreen;//17
    GPIO* ledBlue;//27
    
    int id=1001; //sets what channel and nodes we will be using 
    bool shutdown = false;

public:
    clientClass();
    
    clientClass(int id);
        
    void modeSetter();
    
    void startListening(){
        rxListener = thread(&RF24::startListening, radio);
        

    }
    
    void startModeSetter(){
        modeSetterThread = thread(&clientClass::modeSetter, this);
        
        
    }
    
    void stopModeSetter(){
        modeSetterThread.join();
    }
    
    void stopListening(){
        radio->stopListening();
        rxListener.join();
    }
    
};
