//
//  client.cpp
//  WirelessClearComm
//
//  Created by Gregory Bell on 10/23/18.
//  Copyright © 2018 Gregory Bell. All rights reserved.
//

#include "clientClass.hpp"
#include <iostream>
#include <string>
#include <thread>
using namespace std;
clientClass::clientClass() : rxListener(), modeSetterThread() {
    cout << "INFORMATION: Initalizing client mode" << endl;
    if (id == 1001)//if not yet set, set to the default
        id =1;
    
    uint8_t channelNumber = 123;//If for some reason an error occurs then this is the max I wanna go for channel number
    cout <<"INFORMATION: Initalizing Attenna" << endl;
    switch (id) {
        case 1:
            channelNumber = 63;
            break;
        case 2:
            channelNumber = 68;
            break;
        case 3:
            channelNumber = 73;
            break;
        case 4:
            channelNumber = 78;
            break;
        case 5:
            channelNumber = 83;
            break;
        case 6:
            channelNumber = 88;
            break;
    }

    radio = new RF24(26, 0, BCM2835_SPI_SPEED_32MHZ);//The highest recomended spi speed
    radio->begin();
    radio->setDataRate(RF24_1MBPS );//what speed we want to tranmit
    radio->setChannel(channelNumber);//What channle we want to use
    cout << "INFORMATION: Initilizing misc parts" << endl;
    
    //Activate the misc stuff like a push to talk button and LEDs
    ledRed = new GPIO("4");
    ledGreen = new GPIO("17");
    ledBlue = new GPIO("27");
    
    //Red Pin
    ledRed->exportGPIO();
    ledRed->setDir("out");
    //Green Pin
    ledGreen->exportGPIO();
    ledGreen->setDir("out");
    //Blue Pin
    ledBlue->exportGPIO();
    ledBlue->setDir("out");
    
    cout << "Activating LED" << endl;
    ledBlue->activate();
    ledGreen->activate();
    ledRed->activate();
    
    ledBlue->setGPIO("0");
    ledGreen->setGPIO("1");
    ledRed->setGPIO("0");
    
    
    
    cout << "INFORMATION: Powering up attena..." << endl;
    radio->powerUp();
    radio->printDetails();
    startModeSetter();
    //Power up pin
    //from the ID set the frecuncy for the antenna
    cout << "Ready" << endl;
    startModeSetter();
    
}

clientClass::clientClass(int i) : rxListener(), modeSetterThread(){
    if (id > 6){
        cout <<"ERROR: Invaild ID. Defaulting to 1" << endl;
        i=1;
    }
    id = i;
    clientClass();
}

void clientClass::modeSetter() {
    bool timeOut = false;
    startListening();
    radio->openReadingPipe(id, addresses[id-1]);//Leaving 4 open pipes for bottel neck prevention
    radio->openWritingPipe(addresses[id]);
    while (!radio->failureDetected || !shutdown){
        //code to see if we should wake up
        while (!timeOut && !radio->failureDetected){
            ledBlue->setGPIO("0");
            ledGreen->setGPIO("0");
            ledRed->setGPIO("0");
            //As of right now this is an infinte loop.. Need to add timer.
            if(radio->available()){//If we have an aviable message.
                ledBlue->setGPIO("1");
                ledGreen->setGPIO("0");
                ledRed->setGPIO("0");
                while(radio->available()){//While there is still data to be written call this.
                    radio->read(&byteArray, sizeof(1024));
                }

                if(/* DISABLES CODE */ (false)){//If we have an audio sample that is ready for transmission
                //Perform audio conversion
                //Send to byte array
                //compress
                stopListening();
                if(radio->write(byteArray.data(), sizeof(1024), 1)){//If transmission fails this will return false
                    cout <<"ERROR: Data Packet send failed. No confirmation recieved" << endl;
                    
                    }
                
                }
                startListening();


            
            }
        
        
        }
    }
    if (radio->failureDetected){
        cout << "ERROR: Program is halting....... Please check for crical failures." << endl;
    }
    
    return;
    
    
}





