//
//  masterClass.cpp
//  WirelessClearComm
//
//  Created by Gregory Bell on 11/18/18.
//  Copyright © 2018 Gregory Bell. All rights reserved.
//

#include "masterClass.hpp"


using namespace std;
masterClass::masterClass(){
    uint8_t channelNumber = 123;//If for some reason an error occurs then this is the max I wanna go for channel number
    SYS_MANAGER = false;
    CLEINTS = 1;
    if (id == 1001)
        id = 1;
    if (usbInterface == "DEFAULT")
        usbInterface = "rpi0usb"; //THIS IS TEMPORAY UNTIL I FIND OUT WHAT THE USB INTERFACE IS NAMED
#if defined (raspi) 
    usb = serialInterface(usbInterface, 115200);//Setting our max speed to ~5mbs
#endif
    cout <<"INFORMATION: Initalizing Attenna" << endl;
    switch (id) {
        case 1:
            channelNumber = 63;
            break;
        case 2:
            channelNumber = 68;
            break;
        case 3:
            channelNumber = 73;
            break;
        case 4:
            channelNumber = 78;
            break;
        case 5:
            channelNumber = 83;
            break;
        case 6:
            channelNumber = 88;
            break;
    }
    
    radio = new RF24(26, 0, BCM2835_SPI_SPEED_32MHZ);//The highest recomended spi speed
    radio->begin();
    radio->setDataRate(RF24_1MBPS );//what speed we want to tranmit
    radio->setChannel(channelNumber);//What channle we want to use
    cout << "INFORMATION: Powering up attena..." << endl;
    radio->powerUp();
    radio->printDetails();
    //Power up pin
    //from the ID set the frecuncy for the antenna
    cout << "INFORMATION: Ready" << endl;

    
    
}


masterClass::masterClass(string usbInterface, int id){
    this->id = id;
    this->usbInterface = usbInterface;
    masterClass();//Call up
    
}


masterClass::masterClass(bool isSystemManager, int numOfClients){
    SYS_MANAGER = isSystemManager;
    CLEINTS = numOfClients;
    
    if(SYS_MANAGER&& CLEINTS <=10){
        cleintMeta.resize(CLEINTS);
        for (int i =0; i < cleintMeta.size(); i++){
            cleintMeta[i].push_back("DEFAULT"+to_string(i));//set to default usb port
            cleintMeta[i].push_back("user" + to_string(i)); // The user name
            cleintMeta[i].push_back("unmuted");//marks as unmuted
            cleintMeta[i].push_back("ERROR");//there is an error in the transmission
        }
        cout << "INFORMATION: Set as system manager" << endl;
        cout << "INFORMATION: Starting system manager command line" << endl;
        thread sysManagerCmdThread (&masterClass::sysManagerCommandLine, this);
        cout <<"INFORMATION: A new thread has been spawned with the id " << sysManagerCmdThread.get_id() << ". Please note this can cause issues if not run on a multithreaded processor" << endl;
        sysManagerCmdThread.join();//THE OTHER THREAD MUST HAVE FINHED RUNNING BY THE TIME THE PROGRAM IS RUNNING SO FOR DEBUG RN OTHERWISE IT WILL MAKE ME WANT TO COMMIT DEATH
    }
    else{
        cout << "ERROR: Invalid cleint number entered. Running Defualt constructor" << endl;
        masterClass();//Don't want to rewrite the code so im gonna call up to the defualt
    }
}


masterClass::masterClass(bool isSystemManager, int numOfClients, const vector<vector<string>> clientMetaFromFile){
    SYS_MANAGER = isSystemManager;
    CLEINTS = numOfClients;
    if (SYS_MANAGER&&CLEINTS <=10){
        cleintMeta.resize(CLEINTS);
        if (clientMetaFromFile.size() == CLEINTS){//cleint meta will be a 2d array that
            cout << "INFORMATION: Loading existing meta data...." << endl;
            for (int i =0; i < cleintMeta.size(); i++){
                cleintMeta[i].push_back(clientMetaFromFile[i][0]);
                cleintMeta[i].push_back(clientMetaFromFile[i][1]);
                cleintMeta[i].push_back("unmuted");
                cleintMeta[i].push_back("ERROR");
            }
            
        }
    }
    
}

void masterClass::recieve(){
    if (SYS_MANAGER){
        //do check to see what USB port sent it then broadcast it the others
        //If port is muted then ignore it
    
    }
    else{
        //reencode to wav, then sends to master
    }
    
    return;

}


void masterClass::transmit(uint8_t dataToTransmit){
    if(SYS_MANAGER){
        //sends data to all expect for the one transmitting
        //if defened then ignore
    }
    else {
        //take data from master then send it to the antenna
        
        
    }
    
    return;
    
    
}

void masterClass::mute(string userToMute, string newStatus){
    int indexToEdit = -10;
    for(int i =0; i < cleintMeta.size(); i++){
        if(userToMute == cleintMeta[i][1]){
            indexToEdit = i;
            break;
        }
    }
    if ( indexToEdit != -10){
        cleintMeta[indexToEdit][2] = newStatus;
        cout << "INFORMATION: Wrote " << cleintMeta[indexToEdit][2] << " to the user " << masterClass::cleintMeta[indexToEdit][1] << endl;
        return;
    }
    cout << "ERROR: User \'" << userToMute << "\' not found" << endl;
    
    
    
}

int masterClass::commandHandler(string command){
    if (command == "mute") {
        cout << "Enter the user to mute: ";
        cin>>command;
        mute(command, "mute");
        return 0;
        
    }
    else if (command == "unmute"){
        cout << "Enter the user to unmute: ";
        cin>>command;
        mute(command, "unmute");
        return 0;
    }
    else if (command == "print"){
        for (int i =0; i < cleintMeta.size(); i++){
            for(int j =0; j < cleintMeta[i].size(); j++)
                cout << cleintMeta[i][j] << "  ";
            cout << endl;
            
        }
        return 0;
    }
    
    else if (command == "exit")
        return -1;
    
    else
        return -10;
    
    
}




void masterClass::sysManagerCommandLine(){
    string usrIn = "";
    int attempts =0;
    bool exit = false;
    int result;
    
    cout << "INFORMATION: System manager command line has started on thread: " << this_thread::get_id() << endl;
    
    cout << "Welcome to the system manager command line." << endl;
    cout << "Enter password for ClearCommWirelessUser: ";
    while (!(usrIn == ADMIN_PSSWD) && attempts <= 5){
        usrIn = "";
        attempts++;
        cin >>usrIn;

        if (attempts >= 5){
            cout << "\nMax password attemps exceeded. Please restart to try again." << endl;
            return;//Exiting the funct
        }
        if (usrIn == ADMIN_PSSWD){
            cout << "\nWelcome ClearCommWirelessUser." << endl;
            break;
        }
        else {
            cout << "\nWrong Password" << endl;
        }
        
        
    }
    
    cout <<"User has been verfied. Enter help for a list of commands." << endl;
    
    while(!exit){
        usrIn ="";
        cout << "Enter a command > ";
        cin >>usrIn;
        result=commandHandler(usrIn);
        if (result == -1){
            exit = true;
        }
        
        
        
        
    }
    
}

















