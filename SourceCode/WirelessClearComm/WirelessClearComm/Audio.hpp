//
//  Audio.hpp
//  WirelessClearComm
//
//  Created by Gregory Bell on 12/2/18.
//  Copyright © 2018 Gregory Bell. All rights reserved.
//

#ifndef Audio_hpp
#define Audio_hpp

#include <stdio.h>
#include <iostream>

#include "portaudio.h"



typedef int (PortAudioCallback)( void *inputBuffer,
void *outputBuffer,
unsigned long framesPerBuffer,
PaTimestamp outTime,
void *userData );

using namespace std;
bool isInit = false;

static void init(){
    isInit = true;
    cout << "INFORMATION: Starting Port Audio..." << endl;
    Pa_error err = Pa_Initialize();
    
    if (err != paNoError){
        isInit = false;
        cout << "CRITICAL: Port Audio Failed to initilize with error " << Pa_GetErrorText(err) << endl;
        cout << "INFORMATION: Due to critical failure program will now exit" << endl;
        exit(-100);//Critial failure exit code
    }
    
    err = Pa_open;
    
    

}






#endif /* Audio_hpp */
