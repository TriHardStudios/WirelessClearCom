//
//  masterClass.hpp
//  WirelessClearComm
//
//  Created by Gregory Bell on 11/18/18.
//  Copyright © 2018 Gregory Bell. All rights reserved.
//

#ifndef masterClass_hpp
#define masterClass_hpp

#include <stdio.h>
#include <vector>
#include <string>
#include <thread>
#include <iostream>
#include <termios.h>
#include <unistd.h>
#include "RF24/RF24.h"
#include "SerialInterface.h"

using namespace std;



class masterClass {
private:
    bool SYS_MANAGER; //is this a system manager?
    
    int CLEINTS;//How many connected clients we have
    
    thread rxListener;
    
    RF24 *radio;
#if defined (raspi)
    serialInterface usb;
#endif
    
    int id  = 1001;
    
    string usbInterface = "DEFAULT";
    
    vector<vector<string>> cleintMeta;//the index is number of cleints the first row is the usb port they corispond to then the metadata like where the wcc is it is muted, are we recieving
    //the rpi will only have a max of 4 cleints so I put it to the max as if nothing gets put there the ram ussage will be very small
    const string ADMIN_PSSWD = "ccwpassword";//Super secure I know

    
    
public:    
    masterClass(); //Default Construtor
    
    masterClass(string usbInterface, int id);
    
    masterClass(bool isSystemManager, int numOfClients);//Takes if we are system manager then the number of cleints then init them to the default usb locs
    
    masterClass(bool isSystemManager, int numOfClients, vector<vector<string>> ceientMetaFromFile);//Gets an entire array of meta data
    
    void recieve();
    
    void transmit(uint8_t dataToTransmit);
    
    string getUsbInterface();
    
    void stopListening(){
        radio->stopListening();
        rxListener.join();
    }
    void startListening(){
        rxListener = thread(&RF24::startListening, radio);
        
        
    }
    
    //System Manager
    
    void sysManagerCommandLine();
    
    void mute(string userToMute, string newStatus);
    
    int commandHandler(string command);
    
    //~masterClass();//deconstrutor
    

    
    
    
    
    
    
    
    
    

    
    
    
    
};
#endif /* masterClass_hpp */

