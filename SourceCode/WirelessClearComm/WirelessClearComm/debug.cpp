//
//  mainDebug.cpp
//  WirelessClearComm
//
//  Created by Gregory Bell on 10/21/18.
//  Copyright © 2018 Gregory Bell. All rights reserved.
//

#include "debugClass.hpp"
#include <iostream>
#include <string>
#include "GPIO.hpp"

using namespace std;
//GPIO Pin Prototypes
//These are just memory pointers with nothing on them
static GPIO* pins[40];//Stores the pin pointers and mu secrate jazz stash
static string pinVal[40];//Stores the pin values

/*
 This function initilizes the GPIO header by assigning accual pointers to the array slots
 */
void init(){
    cout << "INFORMATION: Initlizing GPIO header.." << endl;
    for(int i =0; i < 40; i++){
        pins[i] = new GPIO(to_string(i+1));//Initializes the GPIO Pin array
        pinVal[i] = "";//Initializes the val array
        cout << "INFORMATION: Initilized pin " << i+1  << endl;
    }
    cout << "INFORMATION: Initlized GPIO header" << endl;
    
}
/*
 This gets the pin ready for use by setting everything but actual data
 pinNum - the pin that we want to effect
 dir - the direction that we want to set the pin to.
 */
void exportPin(int pinNum, string dir){
    pins[pinNum-1]->exportGPIO();//Tell linux HEYYY We want to activate a pin
                                //This might cause an error if the system we are trying to work with doesn't have a gpio header
    pins[pinNum-1]->setDir(dir);//Tells linux the direction that we want the pin to work in
                               //Again might throw an error if we don't have a GPIO header
    pins[pinNum-1]->activate();//A safegaurd to prevent reading or writing bad data to the pin.
    cout << "INFORMATION: Exported and activated pin " << pinNum << endl;
}
/*
 Pretty much the same as above execpt this one uses the default direction (out) and activates all of them using a for loop
 Errors will be thrown in the same place as above. I'm just lazy so I didn't mark them
 */
void exportPin(){
    for(int i =0; i < 40; i++){
        pins[i]->exportGPIO();//HEYYY We want to activate this pin
        pins[i]->setDir("out");//Sets the defualt direction
        pins[i]->activate();//WE'RE READY!
        cout << "INFORMATION: Exported and activated pin " << i+1  << endl;
    }
}
/*
 Sets the value to the pin
 pinNum - the pin that we want to change
 val - the new value that we want to set must be 1 or 0 unless I write a string to binary class
 */
void setVal(int pinNum, string val){
    pins[pinNum-1]->setGPIO(val);//Sets the pin's data state
}
/*
 Loads the header's data into memory
 */
void getVal(){
    for (int i = 0; i < 40; i++)
        pinVal[i] = pins[i]->getGPIO();//Loads the pin data into the array
}

/*
 The default value printer all it does is print the data on the pin to the console
 */
void printVal(){
    for(int i =0; i <40; i++)
        cout << "Pin " << i+1 << ": " << pinVal[i] << endl;
}
/*
 Prints tha value of a speific pin
 */
void printVal(int pinNum){
    cout << "Pin " << pinNum << ": " << pinVal[pinNum-1] << endl;
}

void unexport(int pinNum) {
    pins[pinNum-1]->deactivate();
    pins[pinNum-1]->unexpoprtGPIO();
    pins[pinNum-1]->~GPIO();
    delete pins[pinNum-1];
}
void unexport() {
    for (int i =0; i < 40; i++){
    pins[i]->deactivate();
    pins[i]->unexpoprtGPIO();
    pins[i]->~GPIO();
    delete pins[i];
    }
}
/*
 Welcome message want to add color soon
 */
void welcome(){
    cout<< "Welcome to the debug tools for Wireless ClearCom. \n"
    <<"For a list of debug commands type help \n"
    << "John S.    --- Project Manager\n"
    << "Gregory B. --- Software Development \n"
    << "Gavin V.   --- System Designer\n"
    << "Sam S.     --- Project Cordinator\n"
    << "copyright 2018 TriHard Studios" << endl;
}
/*
 The comand handler. Doesn't print any thing to the console just returns ints
 0  normal
 10-19 syntex error
 -1 exit
 Each if statment checks what the user has types then will promt for more if nessary
 It also performs logic to check if the data the user has entered is valid
 */
int debugClass::comHandler(string com){
    int pinNum =0;
    string val ="";
    if(com == string("exit")){
        if(!(pins[0] == NULL && pins[36]==NULL))
            return 12;
        else
            return -1;
    }
    else if (com == string("init"))
        init();
    else if (com == string("export")){
        cin >> pinNum;
        cin >> val;
        if (pinNum > 40)
            exportPin();
        else
            exportPin(pinNum, val);
    }
    else if (com == string("set")){
        cin >> pinNum;
        cin >> val;
        if (pinNum > 40 || pinNum <= 0)
            return 11;
        setVal(pinNum, val);
        
    }
    else if (com == string("get")){
        cin >> pinNum;
        getVal();
        if (pinNum > 40)
            printVal();
        else
            printVal(pinNum);
    }
    else if (com == string("unexport")){
        cin >> pinNum;
        getVal();
        if (pinNum > 40)
            unexport();
        else
            unexport(pinNum);
    }
    
    else
        return 10;
    
    
    return 0;
}

/*
 The debug loop. This gets data from the user then passes it the command handler which will then give it a result code.
 */
int debugClass::debug(){
    welcome();
    string com="";
    bool debugging = true;
    int result =0;
    while (debugging){
        com = "";
        cout << "debug console > ";
        cin >> com;
        result = comHandler(com);
        if (result == -1)
            debugging = false;
        else if (result == 12)
            cout << "ERROR: Some pins are still active. Deactivate all pins before exiting" << endl;
        else if (result == 11)
            cout << "ERROR: Invalid Pin" << endl;
        else if (result == 10)
            cout << "ERROR: Command not found" << endl;
    }
    
    
    return 0;
}









