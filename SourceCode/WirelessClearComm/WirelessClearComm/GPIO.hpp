/**
 The GPIO control class. Each object created from this class will control just one pin on the raspberry pi
 Writen by Gregory Bell
 copyright 2018 TriHard Studios
*/
#pragma once
#include <string>


using namespace std;
class GPIO {
public:
    GPIO();//Defualt 
    GPIO(string pinNum);//Constutor
    ~GPIO();//Deconstrutor
    
    //Exporters
    int exportGPIO();//Tells linux that Heyyy we have a pin we would like to use
    
    int unexpoprtGPIO();//Tells linux MY DUDE FUCK OFF THIS PIN
    
    //accesers
    int setDir(string val);//Sets what direction the GPIO we want to go in eg input or output
    
    int setGPIO(string val);//Sets the value
    
    string getGPIO();//Reads the value and returns it.
    
    void activate();//Tells if we can use this pin yet
    
    void deactivate();//turns pin off
    
    string getPin();//Getter method to get the pin number that we are using.
        
private:
    string GPIOPinNum;//Pin number
    bool active = false;//Are we running?
    
    //Directories
    const string exportLoc = "/sys/class/gpio/export";//Where the pin that we are activing is stored
    const string unexportLoc = "/sys/class/gpio/unexport";//Where the pin that we are deactiving is stored
    string pinDirLoc = "/sys/class/gpio/gpio";//This var is finalized at object creation 
    string pinValueLoc = "/sys/class/gpio/gpio";
    
};



