#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include "GPIO.hpp"

using namespace std;
GPIO::GPIO(){
    cout << "INFORMATION: Set GPIO pin num to NULL" << endl;
}

GPIO::GPIO(string pinNum){
    GPIOPinNum = pinNum;
    cout << "INFORMATION: Set GPIO pin num to " << GPIOPinNum << endl;
    pinDirLoc += GPIOPinNum + "/direction";
    pinValueLoc += GPIOPinNum + "/value";
    cout << "INFORMATION: Set paths for GPIO pin " << GPIOPinNum << endl;
    cout << "INFROMATION: Pin direction directory set " << pinDirLoc << endl;
    cout << "INFROMATION: Pin value directory set " << pinValueLoc << endl;
    
}


int GPIO::exportGPIO(){
    ofstream exportGPIO(exportLoc.c_str());
    if (!exportGPIO.good()){
        cout << "FATEL: Failed to open GPIO file" << endl;
        return -1;
    }
    
    exportGPIO << this-> GPIOPinNum;
    cout << "INFORMATION: Exported GPIO pin" << endl;
    exportGPIO.close();
    return 0;
}

int GPIO::unexpoprtGPIO() {
    if(!active){
    ofstream unexportGPIO(unexportLoc.c_str());
    if(!unexportGPIO.good()){
        cout << "FATEL: Failed to open GPIO file" << endl;
        return -1;
    }
    
    unexportGPIO << this->GPIOPinNum;
    cout << "INFORMATION: Unexported GPIO pin" << endl;
    unexportGPIO.close();
    return 0;
    }else {
        cout << "ERROR pin is still active. Deactiavte before unexporting" << endl;
        return -2;
    }
}


int GPIO::setDir(string val){
    ofstream setDirGPIO(pinDirLoc.c_str());
    if (!setDirGPIO.good()){
        cout << "ERROR: Failed to open GPIO file" << endl;
        return -1;
    }
    setDirGPIO << val;
    cout << "INFORMATION: Wrote new direction data to pin " + this->GPIOPinNum << endl;
    setDirGPIO.close();
    return 0;
    
    
}

int GPIO::setGPIO(string val){
    if(active){
        ofstream setValGPIO(pinValueLoc.c_str());
        if (!setValGPIO.good()){
            cout << "ERROR: Failed to open GPIO file" << endl;
            return -1;
        }
        setValGPIO << val;
        cout << "INFORMATION: Wrote new data to pin " + this->GPIOPinNum << endl;
        setValGPIO.close();
        return 0;
    } else{
        cout << "ERROR: Pin is not active" << endl;
        return -2;
    }
    
    
}

string GPIO::getGPIO(){
    if (!active)
        return "-1";
    string val = "";
    ifstream getValGPIO(pinValueLoc.c_str());
    if(!getValGPIO.good()){
        cout << "ERROR: Failed to read GPIO file" << endl;
        return "-1";
    }
    
    getValGPIO >> val;
    
    if(val == string("0"))
        val = "1";
    else
        val = "0";
    
    return val;
}

void GPIO::activate(){
    this->active= true;
    cout <<"WARNING: GPIO pin " << getPin() << " is now active" << endl;
}

void GPIO::deactivate(){
    this->active = false;
    cout << "INFORMATION: GPIO pin " << getPin() << " is deactivated" << endl;
}

string GPIO::getPin(){
    return this->GPIOPinNum;
}

GPIO::~GPIO(){
    cout << "INFORMATION: Shutting down GPIO service" << endl;
    //Prolly unnessary
    if(!active){
        this->GPIOPinNum = "\0";
        cout << "INFORMATION: Cross your fingers... Object is being deleted" << endl;
        //delete this;
        return;
    }
    deactivate();
    unexpoprtGPIO();
    this->GPIOPinNum = "\0";
    cout << "INFORMATION: Cross your fingers... Object is being deleted" << endl;
    //delete this;
}
